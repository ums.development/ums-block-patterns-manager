<?php

namespace UMS\BlockPatternsManager\Options;

use UMS\BlockPatternsManager\Services\Contexts;

/**
 * Class WPMUOptions
 * @package UMS\BlockPatternsManager\Options
 */
class WPMUOptions {
	
	/**
	 *
	 */
	const OPTIONS_NAME = 'ums_patterns_manager_options';
	/**
	 *
	 */
	const OPT_CONTEXTS = 'contexts';
	const OPT_REMOVE_CORE_PATTERNS = 'remove_core_block_patterns';
	
	/**
	 * WPMUOptions constructor.
	 */
	public function __construct() {
		add_action( 'wpmu_options', [ $this, 'render' ] );
		add_action( 'update_wpmu_options', [ $this, 'update' ] );
	}
	
	/**
	 * @return mixed|null
	 */
	public static function getOptSelectedContexts(): array {
		return self::getOption( self::OPT_CONTEXTS, [] );
	}
	
	/**
	 * @param $optionName
	 * @param mixed|null $ifNull
	 *
	 * @return mixed|null
	 */
	public static function getOption( $optionName, $ifNull = null ) {
		$options = self::getOptions();
		
		return $options[ $optionName ] ?? $ifNull;
	}
	
	/**
	 * @return array|mixed
	 */
	public static function getOptions() {
		
		$options = get_network_option( 0, self::OPTIONS_NAME );
		
		return $options ? json_decode( $options, true ) : [];
		
	}
	
	/**
	 *
	 */
	public function render() {
		
		$contextsArr = [];
		
		$inputNames = self::OPTIONS_NAME;
		
		$selectedContexts = self::getOption( self::OPT_CONTEXTS, [] );
		
		foreach ( Contexts::getContexts() as $context ) {
			
			$context = htmlentities( $context, ENT_QUOTES );
			
			$id      = "$inputNames-contexts-$context";
			$checked = in_array( $context, $selectedContexts ) ? ' checked ' : ' ';
			
			/** @noinspection PhpUnnecessaryCurlyVarSyntaxInspection They are, actually. */
			$contextsArr[] = "
				<input name='{$inputNames}[" . self::OPT_CONTEXTS . "][]' type='checkbox' id='$id' value='$context' $checked>
				<label for='$id'>$context</label>
			";
		}
		$contexts = implode( '<br>', $contextsArr );
		
		$rmCoreChecked = WPMUOptions::getOptRemoveCorePatterns() ? ' checked ' : ' ';
		
		/** @noinspection PhpUnnecessaryCurlyVarSyntaxInspection  They really are needed */
		echo "
			<h2>UMS Patterns Manager</h2>
			<hr>
			<table class='form-table' role='presentation'>
				<tbody>
					
					<!-- Remove Core Patterns -->
					<tr>
						<th scope='row'>
							<label for='$inputNames-" . self::OPT_REMOVE_CORE_PATTERNS . "'>
								Remove Built In Block Patterns
							</label>
						</th>
						<td>
							<input 
								name='{$inputNames}[" . self::OPT_REMOVE_CORE_PATTERNS . "]'
								type='checkbox'
								id='$inputNames-" . self::OPT_REMOVE_CORE_PATTERNS . "'
								value='1'
								$rmCoreChecked
							>
						</td>
					</tr>
					
					<!-- Enabled Contexts -->
					<tr>
						<th scope='row'>
							<label for='$inputNames-" . self::OPT_CONTEXTS . "'>Enabled Contexts</label>
						</th>
						<td>
							<fieldset id='$inputNames-" . self::OPT_CONTEXTS . "'>
								$contexts
							</fieldset>
						</td>
					</tr>
					
				</tbody>
			</table>
		";
	}
	
	/**
	 * @return bool
	 */
	public static function getOptRemoveCorePatterns(): bool {
		return (bool) self::getOption( self::OPT_REMOVE_CORE_PATTERNS, false );
	}
	
	/**
	 *
	 */
	public function update() {
		
		$jsonPostOptions = json_encode( [
			self::OPT_CONTEXTS             => $_POST[ self::OPTIONS_NAME ][ self::OPT_CONTEXTS ],
			self::OPT_REMOVE_CORE_PATTERNS => $_POST[ self::OPTIONS_NAME ][ self::OPT_REMOVE_CORE_PATTERNS ],
		] );
		
		update_network_option( 0, self::OPTIONS_NAME, $jsonPostOptions );
	}
}

