<?php

namespace UMS\BlockPatternsManager;

use UMS\BlockPatternsManager\Options\WPMUOptions;

/**
 * Class Main
 * @package UMS\BlockPatternsManager
 */
class Main {
	
	/**
	 *
	 */
	const PATTERNS_ROOT_DIR = UMS_BLOCK_PATTERNS_MANAGER_PLUGIN_DIR . '/src/PatternSets/';
	
	/**
	 *
	 */
	const PATTERNS_ROOT_NAMESPACE = 'UMS\BlockPatternsManager\PatternSets\\';
	
	/**
	 *
	 */
	public function run() {
		
		add_action( 'init', [ $this, 'registerPatterns' ] );
		add_action( 'after_setup_theme', [ $this, 'removeCoreBlockPatterns' ] );
		
		if ( is_admin() ) {
			new WPMUOptions();
		}
	}
	
	/**
	 *
	 */
	public function removeCoreBlockPatterns() {
		if ( WPMUOptions::getOptRemoveCorePatterns() ) {
			remove_theme_support( 'core-block-patterns' );
		}
	}
	
	/**
	 *
	 */
	public function registerPatterns() {
		
		$selectedContexts = WPMUOptions::getOptSelectedContexts();
		
		foreach ( $selectedContexts as $context ) {
			
			$srcDir           = self::PATTERNS_ROOT_DIR . $context;
			$contextNamespace = self::PATTERNS_ROOT_NAMESPACE . $context . '\\';
			
			foreach ( glob( $srcDir . '\/*.php' ) as $filename ) {
				
				$nakedFileName = pathinfo( $filename, PATHINFO_FILENAME );
				
				/** @var BlockPatternInterface|string $fullClassName */
				$fullClassName = $contextNamespace . $nakedFileName;
				
				$categories = $fullClassName::getCategories();
				foreach ( $categories as $category ) {
					
					$catName = str_replace( '_', ' ', $category );
					$catName = ucwords( $catName );
					register_block_pattern_category( $category, [ 'label' => $catName ] );
				}
				
				register_block_pattern(
					UMS_BLOCK_PATTERNS_MANAGER_PLUGIN_SLUG . '/' . $fullClassName::getSlug(),
					self::getPropertiesFromBlockPatternInterface( $fullClassName )
				);
			}
		}
	}
	
	/**
	 * @param string|BlockPatternInterface $class
	 *
	 * @return array
	 */
	private static function getPropertiesFromBlockPatternInterface( $class ): array {
		$rtn = [
			'title'       => $class::getTitle(),
			'description' => $class::getDescription(),
			'content'     => $class::getContent(),
			'categories'  => $class::getCategories(),
			'keywords'    => $class::getKeywords(),
		];
		
		if ( $class::getViewportWidth() ) {
			$rtn['viewportWidth'] = $class::getViewportWidth();
		}
		
		return $rtn;
	}
}