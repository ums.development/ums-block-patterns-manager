<?php

namespace UMS\BlockPatternsManager;

/**
 * Interface BlockPatternInterface
 * @package UMS\BlockPatternsManager
 */
interface BlockPatternInterface {
	/**
	 * @return string
	 */
	public static function getSlug(): string;
	
	/**
	 * @return string
	 */
	public static function getTitle(): string;
	
	/**
	 * @return string
	 */
	public static function getDescription(): string;
	
	/**
	 * @return string
	 */
	public static function getContent(): string;
	
	/**
	 * @return array
	 */
	public static function getCategories(): array;
	
	/**
	 * @return array
	 */
	public static function getKeywords(): array;
	
	/**
	 * @return int|null
	 */
	public static function getViewportWidth(): ?int;
	
}