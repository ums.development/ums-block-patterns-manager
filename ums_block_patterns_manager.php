<?php
/**
 * Plugin Name: UMS Block Patterns Manager (Public)
 * Plugin URI: https://gitlab.its.maine.edu/wp/ums-block-patterns-manager
 * Description: WordPress Plugin for choosing a set of block patterns to be used on all sub-sites across a multisite network, and/or disabling the core block patterns included with WordPress.
 * Version: 1.3.0
 * Author: UMS US:IT Web Development Team
 * Author URI: https://gojira.its.maine.edu/confluence/display/WB/US%3AIT+Web+Applications+Development+Team+Home
 * License: GPL v2 or later
 *
 * @package UMS Block Patterns Manager
 */

use UMS\BlockPatternsManager\Main;

require_once 'vendor/autoload.php';

const UMS_BLOCK_PATTERNS_MANAGER_PLUGIN_SLUG = 'ums_block_patterns';
const UMS_BLOCK_PATTERNS_MANAGER_PLUGIN_DIR = __DIR__;

$umsBlockPatternsManager = new Main();
$umsBlockPatternsManager->run();