# UMS Block Patterns Manager (Public Release)
 
University of Maine System IT, Custom Enterprise Solutions, Web Development Team

https://www.maine.edu/

WordPress Plugin for choosing a set of block patterns to be used on all sub-sites across a multisite network, and/or disabling the core block patterns included with WordPress.

Plugin must be network activated. Settings are a section of the Network Settings page

## Block Pattern Definitions

Block patterns are defined with classes in the sub-folders within the `src/PatternSets` directory which implement the BlockPatternInterface class. Each sub-folder is referred to as a `context` in the settings page and can be enabled or disabled as a whole. 

The BlockPatternInterface class declares a getViewportWidth() method. If a block pattern does not wish to define a viewportWidth then that method should return `null`.

Future versions of this plugin will probably move block pattern definitions to a database content objects.

## Development

Use composer to install dependencies and deploy the entire directory (`ums-block-patterns-manager`) to `/wp-content/plugins/ums-block-patterns-manager`

## Release

When ready, zip the entire project directory (including the composer generated vendor folder), minus any IDE related project files. 

## Change Log

1.3.0 - Public Branch - Sanitized for public release  

1.2.0
* Internal changes for the University of Maine System

1.1.0
* Internal changes for the University of Maine System

1.0.0
* Initial release for University of Maine System

## License

GPL v2.0 or later
