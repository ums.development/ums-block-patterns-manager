<?php /** @noinspection PhpUnused */

namespace UMS\BlockPatternsManager\PatternSets\SamplePatternSet1;

use UMS\BlockPatternsManager\BlockPatternInterface;

/**
 * Class SampleParagraphPattern
 *
 * @package UMS\BlockPatternsManager\PatternSets\MaineLaw
 */
class SampleParagraphPattern implements BlockPatternInterface {

	/**
	 * @return string
	 */
	public static function getSlug(): string {
		return 'paragraph_pattern_slug';
	}

	/**
	 * @return string
	 */
	public static function getTitle(): string {
		return 'Paragraph Sample';
	}

	/**
	 * @return string
	 */
	public static function getDescription(): string {
		return 'A simple paragraph pattern example';
	}

	/**
	 * @return string
	 */
	public static function getContent(): string {
		return '<!-- wp:paragraph {"style":{"color":{"background":"#002950","text":"#fbfbfb"},"typography":{"fontSize":"21px"}}} -->
<p class="has-text-color has-background" style="background-color:#002950;color:#fbfbfb;font-size:21px">This is nothing but a basic paragraph. It is here just to show you that what you put into the getContent section of the pattern code between single quotes that you copy from the List View is what will end up in the pattern. For example, this paragraph should appear already styled at 21px high and with dark blue background and light font face color. We hope you enjoy the UMS Block Patterns Manager!</p>
<!-- /wp:paragraph -->';
	}

	/**
	 * @return string[]
	 */
	public static function getCategories(): array {
		return array( 'Text Category' );
	}

	/**
	 * @return string[]
	 */
	public static function getKeywords(): array {
		return array( 'text', 'paragraph', 'goodkeywords' );
	}

	/**
	 * @return int|null
	 */
	public static function getViewportWidth(): ?int {
		return null;
	}
}