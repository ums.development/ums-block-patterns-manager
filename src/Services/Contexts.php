<?php

namespace UMS\BlockPatternsManager\Services;

use UMS\BlockPatternsManager\Main;

/**
 * Class Contexts
 * @package UMS\BlockPatternsManager\Services
 */
class Contexts {
	
	/**
	 * @return array
	 */
	public static function getContexts(): array {
		
		$contextDirs = [];
		
		$patternsDirDirs = scandir( Main::PATTERNS_ROOT_DIR );
		foreach ( $patternsDirDirs as $filename ) {
			$fullPath = Main::PATTERNS_ROOT_DIR . "/$filename";
			
			if ( in_array( $filename, [ '.', '..' ] ) || ! is_dir( $fullPath ) ) {
				continue;
			}
			$contextDirs[] = $filename;
		}
		
		return $contextDirs;
	}
	
}